defmodule Contento.Repo.Migrations.CreateRoleToPermissions do
  use Ecto.Migration

  def change do
    create table(:roletopermissions) do
      add :role_id, :integer, default: 0
      add :permission_id, :integer, default: 0
      timestamps()
    end
  end
end
