defmodule Contento.Repo.Migrations.CreatePermissions do
  use Ecto.Migration

  def change do
    create table(:permissions) do
      add :view, :string
      add :read, :boolean, default: false
      add :add, :boolean, default: false
      add :edit, :boolean, default: false
      add :remove, :boolean, default: false
      timestamps()
    end
  end
end
