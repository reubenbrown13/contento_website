defmodule Contento.Repo.Migrations.CreateUserToRoles do
  use Ecto.Migration

  def change do
    create table(:usertoroles) do
      add :user_id, :integer, default: 0
      add :role_id, :integer, default: 0
      timestamps()
    end
  end
end
