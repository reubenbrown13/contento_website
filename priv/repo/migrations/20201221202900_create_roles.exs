defmodule Contento.Repo.Migrations.CreateRoles do
  use Ecto.Migration

  def change do
    create table(:roles) do
      add :name, :string
      add :handle, :string
      timestamps()
    end
  end
end
