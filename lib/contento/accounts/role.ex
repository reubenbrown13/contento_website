defmodule Contento.Accounts.Role do
  use Ecto.Schema

  import Ecto.Changeset

  alias Contento.Accounts.Role
  alias Contento.Accounts.{UserToRole, RoleToPermission}

  @required_fields ~w(name handle)a
  #@optional_fields ~w(website bio location password password_hash activation_token joined_at last_login_at)a
  @optional_fields ~w()a

  schema "roles" do
    field :name, :string
    field :handle, :string

    #has_many :usertoroles, UserToRole, foreign_key: :role_id
    #has_many :roletopermissions, RoleToPermission, foreign_key: :role_id
    timestamps()
  end

  @doc false
  def changeset(%Role{} = role, attrs) do
    role
    |> cast(attrs, @required_fields ++ @optional_fields)
    |> validate_required(@required_fields)
    |> unique_constraint(:name)
    |> unique_constraint(:handle)
    #|> validate_format(:handle, ~r[a-Z0-9])
  end
end
