defmodule Contento.Accounts.Permission do
  use Ecto.Schema

  import Ecto.Changeset

  alias Contento.Accounts.Permission
  alias Contento.Accounts.{RoleToPermission}

  @required_fields ~w(view)a
  @optional_fields ~w(read add edit remove)a

  schema "permissions" do
    field :view, :string
    field :read, :boolean, default: false
    field :add, :boolean, default: false
    field :edit, :boolean, default: false
    field :remove, :boolean, default: false

    #has_many :roletopermissions, RoleToPermission, foreign_key: :permission_id
    timestamps()
  end

  @doc false
  def changeset(%Permission{} = permission, attrs) do
    permission
    |> cast(attrs, @required_fields ++ @optional_fields)
    |> validate_required(@required_fields)
    |> unique_constraint(:view)
    #|> validate_format(:view, ~r[a-Z0-9])
  end
end
