defmodule Contento.Accounts.UserToRole do
  use Ecto.Schema

  import Ecto.Changeset

  alias Contento.Accounts.UserToRole
  alias Contento.Accounts.{User, Role}

  @required_fields ~w(user_id role_id)a

  schema "usertoroles" do
    belongs_to :user, Contento.Accounts.User
    belongs_to :role, Contento.Accounts.Role
    timestamps()
  end

  @doc false
  def changeset(%UserToRole{} = usertorole, attrs) do
    usertorole
    |> cast(attrs, @required_fields)
  end
end
