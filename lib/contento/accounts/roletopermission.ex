defmodule Contento.Accounts.RoleToPermission do
  use Ecto.Schema

  import Ecto.Changeset

  alias Contento.Accounts.RoleToPermission
  alias Contento.Accounts.{Role, Permission}

  @required_fields ~w(role_id permission_id)a

  schema "roletopermissions" do
    belongs_to :role, Contento.Accounts.Role
    belongs_to :permission, Contento.Accounts.Permission
    timestamps()
  end

  @doc false
  def changeset(%RoleToPermission{} = roletopermission, attrs) do
    roletopermission
    |> cast(attrs, @required_fields)
  end
end
