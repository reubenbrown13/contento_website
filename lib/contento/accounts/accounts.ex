defmodule Contento.Accounts do
  @moduledoc """
  The Accounts context.
  """

  import Ecto.Query, warn: false

  alias Contento.Repo
  alias Contento.Accounts.User
  alias Contento.Accounts.Role
  alias Contento.Accounts.Permission

  def get_user(id) when is_binary(id), do: get_user(id: id)
  def get_user(conds) when is_list(conds), do: Repo.get_by(User, conds)
  def get_user(user) when is_map(user), do: Repo.preload(user, :role)
  def get_user(nil), do: nil

  def list_users(pagination_params, conds \\ []) do
    User
    |> where(^conds)
    |> order_by(desc: :inserted_at)
    #|> preload(:role)
    |> Repo.paginate(pagination_params)
  end

  def list_user_handles(conds \\ []) do
    User
    |> select([:handle,:id,:email])
    #from(u in User, select: [:handle,:id,:email])
    |> where(^conds)
    |> order_by(asc: :handle)
    |> Repo.all
    #|> Enum.map(fn(user_list) -> struct(User, user_list) end)
    #users = from(u in User, select: [:handle,:id])
    #|> where(^conds)
    #|> order_by(asc: :handle)
    #|> Repo.all()
  end

  def create_user(attrs \\ %{}) do
    %User{}
    |> User.changeset(attrs)
    |> Repo.insert()
  end

  def create_user_with_activation(attrs \\ %{}) do
    %User{}
    |> User.changeset_with_activation(attrs)
    |> Repo.insert()
  end

  def update_user(%User{} = user, attrs) do
    user
    |> User.changeset(attrs)
    |> Repo.update()
  end

  def delete_user(%User{} = user) do
    Repo.delete(user)
  end

  def change_user(%User{} = user), do: User.changeset(user, %{})

  def change_user_with_activation(%User{} = user), do: User.changeset_with_activation(user, %{})

  def password_correct?(%User{} = user, password), do: Comeonin.Argon2.checkpw(password, user.password_hash)

  def count_users do
    Repo.aggregate(from(u in User), :count, :id)
  end

  def activate_user(token) do
    if user = get_user(activation_token: token) do
      user
      |> User.changeset(%{})
      |> User.activate()
      |> Repo.update()
    else
      {:error, :user_does_not_exist}
    end
  end

  def user_active?(user) do
    user.joined_at && user.password_hash
  end

  def update_user_login_at(user) do
    update_user(user, %{
      last_login_at: NaiveDateTime.truncate(Timex.to_naive_datetime(Timex.now()), :second)
    })
  end

  def get_role(id) when is_binary(id), do: get_role(id: id)
  def get_role(conds) when is_list(conds), do: Repo.get_by(Role, conds)
  def get_role(role) when is_map(role), do: Repo.preload(role, :user, :permission)
  def get_role(nil), do: nil

  def list_roles(pagination_params, conds \\ []) do
    Role
    |> where(^conds)
    |> order_by(desc: :inserted_at)
    #> preload(:user)
    #|> preload(:permission)
    |> Repo.paginate(pagination_params)
  end

  def create_role(attrs \\ %{}) do
    %Role{}
    |> Role.changeset(attrs)
    |> Repo.insert()
  end

  def update_role(%Role{} = role, attrs) do
    role
    |> Role.changeset(attrs)
    |> Repo.update()
  end

  def delete_role(%Role{} = role) do
    Repo.delete(role)
  end

  def change_role(%Role{} = role), do: Role.changeset(role, %{})

  def count_roles do
    Repo.aggregate(from(r in Role), :count, :id)
  end

  def get_permission(id) when is_binary(id), do: get_permission(id: id)
  def get_permission(conds) when is_list(conds), do: Repo.get_by(Permission, conds)
  def get_permission(permission) when is_map(permission), do: Repo.preload(permission, :tolr)
  def get_permission(nil), do: nil

  def list_permissions(pagination_params, conds \\ []) do
    Permission
    |> where(^conds)
    |> order_by(desc: :inserted_at)
    #|> preload(:role)
    |> Repo.paginate(pagination_params)
  end

  def create_permission(attrs \\ %{}) do
    %Permission{}
    |> Permission.changeset(attrs)
    |> Repo.insert()
  end

  def update_permission(%Permission{} = permission, attrs) do
    permission
    |> Permission.changeset(attrs)
    |> Repo.update()
  end

  def delete_permission(%Permission{} = permission) do
    Repo.delete(permission)
  end

  def change_permission(%Permission{} = permission), do: Permission.changeset(permission, %{})

  def count_permissions do
    Repo.aggregate(from(p in Permission), :count, :id)
  end
end
