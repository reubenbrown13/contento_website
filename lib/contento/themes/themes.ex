defmodule Contento.Themes do
  @moduledoc """
  The Themes context.
  """

  require Logger

  import Ecto.Query, warn: false

  alias Contento.Repo
  alias Contento.Themes.Theme

  def get_theme(id) when is_binary(id), do: get_theme(id: id)
  def get_theme(conds) when is_list(conds), do: Repo.get_by(Theme, conds)

  def list_themes() do
    Repo.all(Theme, order_by: [desc: :inserted_at])
  end
  def list_themes(pagination_params, conds \\ []) do
    Theme
    |> where(^conds)
    |> order_by(desc: :inserted_at)
    |> Repo.paginate(pagination_params)
  end

  def create_theme(attrs \\ %{}) do
    if upload = attrs["theme_file"] do
      extension = Path.extname( upload.filename )
      # upDir = "#{Path.absname("priv/themes")}/#{Path.basename(upload.filename, extension)}/"
      upDir = "#{Path.absname("priv/themes")}/#{String.downcase(String.replace(attrs["name"], ~r/\W/, "_" ))}/"
      File.rm_rf( upDir )
      File.mkdir_p( upDir )
      upFile = String.to_charlist("#{upDir}#{upload.filename}")
      File.cp(upload.path, upFile)
      upFile |> File.chmod!(777)
      upFile |> :zip.extract |> (fn({:ok, files}) -> files end).()
      upFile |> File.rm
    end

    %Theme{}
    |> Theme.changeset(attrs)
    |> Repo.insert()
  end

  def update_theme(%Theme{} = theme, attrs) do
    upload = attrs["theme_file"]
    upDir = "#{Path.absname("priv/themes")}/#{String.downcase(String.replace(attrs["name"], ~r/\W/, "_" ))}/"
    if (false == File.exists?(upDir)) || (String.downcase(String.replace(theme["name"], ~r/\W/, "_" )) == String.downcase(String.replace(attrs["name"], ~r/\W/, "_" ))) do
      upFile = String.to_charlist("#{upDir}#{upload.filename}")
      File.cp(upload.path, upFile)
      upFile |> File.chmod!(777)
      upFile |> :zip.extract |> (fn({:ok, files}) -> files end).()
      upFile |> File.rm
      
      theme
      |> Theme.changeset(attrs)
      |> Repo.update()
    end
  end

  def delete_theme(%Theme{} = theme) do
    # need to delete the theme files also.
    # themeData = get_theme(theme)
    # upDir = "#{Path.absname("priv/themes")}/#{String.downcase(String.replace(themeData["name"], ~r/\W/, "_" ))}/"
    # File.rm_rf( upDir )
    Repo.delete(theme)
  end

  def change_theme(%Theme{} = theme), do: Theme.changeset(theme, %{})

  def count_themes do
    Repo.aggregate(from(p in Theme), :count, :id)
  end
end
