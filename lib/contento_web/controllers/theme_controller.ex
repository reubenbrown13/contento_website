defmodule ContentoWeb.ThemeController do
  use ContentoWeb, :controller

  alias Contento.Themes
  alias Contento.Themes.Theme

  def index(conn, params) do
    data = Themes.list_themes(params)

    render(conn, "index.html", themes: data.entries, page_number: data.page_number,
                               page_size: data.page_size, total_pages: data.total_pages,
                               total_entries: data.total_entries)
  end
  
  def new(conn, _params) do
    changeset = Themes.change_theme(%Theme{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"theme" => theme_params}) do
    with {:ok, %Theme{} = _theme} <- Themes.create_theme(theme_params) do
      conn
      |> put_flash(:info, :created)
      |> redirect(to: admin_theme_path(conn, :index))
    else
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    if theme = Themes.get_theme(id) do
      changeset = Themes.change_theme(theme)
      render(conn, "show.html", theme: theme, changeset: changeset)
    end
  end

  def update(conn, %{"id" => id, "theme" => theme_params}) do
    if theme = Themes.get_theme(id) do
      with {:ok, %Theme{} = theme} <- Themes.update_theme(theme, theme_params) do
        conn
        |> put_flash(:info, :updated)
        |> redirect(to: admin_theme_path(conn, :show, theme))
      else
        {:error, %Ecto.Changeset{} = changeset} ->
          render(conn, "show.html", theme: theme, changeset: changeset)
      end
    end
  end

  def delete(conn, %{"id" => id}) do
    if theme = Themes.get_theme(id) do
      with {:ok, _theme} = Themes.delete_theme(theme) do
        conn
        |> put_flash(:info, :deleted)
        |> redirect(to: admin_theme_path(conn, :index))
      end
    end
  end
end
