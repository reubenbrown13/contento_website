defmodule ContentoWeb.RoleController do
  use ContentoWeb, :controller

  alias Contento.Accounts
  alias Contento.Accounts.Role

  def index(conn, params) do
    data = Accounts.list_roles(params)

    render(conn, "index.html", roles: data.entries, page_number: data.page_number,
                               page_size: data.page_size, total_pages: data.total_pages,
                               total_entries: data.total_entries)
  end

  def new(conn, _params) do
    permissions = Accounts.list_permissions([])
    users = Accounts.list_users([])
    # users = []
    changeset = Accounts.change_role(%Role{})
    render(conn, "new.html", users: users, permissions: permissions, changeset: changeset)
    # render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"role" => role_params}) do
    put_users(role_params, current_resource(conn))
    role_params = put_permissions(role_params, current_resource(conn))

    with {:ok, %Role{} = _role} <- Accounts.create_role(role_params) do
      # create links to users and permissions
      conn
      |> put_flash(:info, :created)
      |> redirect(to: admin_role_path(conn, :index))
    else
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    permissions = Accounts.list_permissions([])
    users = Accounts.list_user_handles([])
    # need to create an Enum for the user's name and id to pass to the form
    if role = Accounts.get_role(id) do
      changeset = Accounts.change_role(role)
      render(conn, "show.html", role: role, users: users, permissions: permissions, changeset: changeset)
      # render(conn, "show.html", role: role, changeset: changeset)
    end
  end

  def update(conn, %{"id" => id, "role" => role_params}) do
    permissions = Accounts.list_permissions([])
    users = Accounts.list_user_handles([])
    if role = Accounts.get_role(id) do
      # update links to users and permissions
      with {:ok, %Role{} = role} <- Accounts.update_role(role, role_params) do
        conn
        |> put_flash(:info, :updated)
        |> redirect(to: admin_role_path(conn, :show, role))
      else
        {:error, %Ecto.Changeset{} = changeset} ->
          render(conn, "show.html", role: role, users: users, permissions: permissions, changeset: changeset)
          # render(conn, "show.html", role: role, changeset: changeset)
      end
    end
  end

  def delete(conn, %{"id" => id}) do
    if role = Accounts.get_role(id) do
      # remove links to users and permissions
      with {:ok, _role} = Accounts.delete_role(role) do
        conn
        |> put_flash(:info, :deleted)
        |> redirect(to: admin_role_path(conn, :index))
      end
    end
  end

  defp put_users(params, users) do
    # need to loop over the users Enum
    Map.merge(params, %{"user_id" => users.id})
  end

  defp put_permissions(params, permissions) do
    # need to loop over the permissions Enum
    Map.merge(params, %{"permission_id" => permissions.id})
  end
end
