defmodule ContentoWeb.WebsiteController do
  use ContentoWeb, :controller

  alias Contento.Content
  alias Contento.Accounts

  action_fallback ContentoWeb.FallbackController

  def index(conn, _params) do
    posts = Content.list_posts(published: true)
    render(conn, template(conn, "index.html"), posts: posts)
  end

  def page_or_post(conn, %{"slug" => slug} = _params) do
    cond do
      page = Content.get_page(slug: slug) ->
        render(conn, template(conn, "page.html"), page: page)
      post = Content.get_post(slug: slug) ->
        data = Content.list_posts(_params)
        render(conn, template(conn, "post.html"), post: post, posts: data.entries)
      true ->
        {:error, :website_not_found}
    end
  end

  def show_page(conn, %{"slug" => slug} = _params) do
    page = Content.get_page(slug: slug)
    render(conn, template(conn, "page.html"), page: page)
  end

  def show_post(conn, %{"slug" => slug} = _params) do
    data = Content.list_posts(_params)
    post = Content.get_post(slug: slug)
    render(conn, template(conn, "post.html"), post: post, posts: data.entries)
  end

  def show_author(conn, %{"handle" => handle} = _params) do
      user = Accounts.get_user(handle: handle)
      data = Content.list_posts([],[author_id: user.id])
      render(conn, template(conn, "author.html"), user: user, posts: data.entries)
  end
  
  defp template(conn, template) do
    conn.assigns[:settings].theme.alias <> "/templates/" <> template
  end
end
