defmodule ContentoWeb.PermissionController do
  use ContentoWeb, :controller

  alias Contento.Accounts
  alias Contento.Accounts.Permission

  def index(conn, params) do
    data = Accounts.list_permissions(params)

    render(conn, "index.html", permissions: data.entries, page_number: data.page_number,
                               page_size: data.page_size, total_pages: data.total_pages,
                               total_entries: data.total_entries)
  end

  def new(conn, _params) do
    roles = Accounts.list_roles([])
    changeset = Accounts.change_permission(%Permission{})
    render(conn, "new.html", roles: roles.entries, changeset: changeset)
  end

  def create(conn, %{"permission" => permission_params}) do
    permission_params = put_roles(permission_params, current_resource(conn))

    with {:ok, %Permission{} = _permission} <- Accounts.create_permission(permission_params) do
      # create links to roles
      conn
      |> put_flash(:info, :created)
      |> redirect(to: admin_permission_path(conn, :index))
    else
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    roles = Accounts.list_roles([])
    if permission = Accounts.get_permission(id) do
      changeset = Accounts.change_permission(permission)
      render(conn, "show.html", roles: roles.entries, permission: permission, changeset: changeset)
    end
  end

  def update(conn, %{"id" => id, "permission" => permission_params}) do
    roles = Accounts.list_roles([])
    if permission = Accounts.get_permission(id) do
      # update links to roles
      with {:ok, %Permission{} = permission} <- Accounts.update_permission(permission, permission_params) do
        conn
        |> put_flash(:info, :updated)
        |> redirect(to: admin_permission_path(conn, :show, permission))
      else
        {:error, %Ecto.Changeset{} = changeset} ->
          render(conn, "show.html", roles: roles.entries, permission: permission, changeset: changeset)
      end
    end
  end

  def delete(conn, %{"id" => id}) do
    if permission = Accounts.get_permission(id) do
      # delete links to roles
      with {:ok, _permission} = Accounts.delete_permission(permission) do
        conn
        |> put_flash(:info, :deleted)
        |> redirect(to: admin_permission_path(conn, :index))
      end
    end
  end

  defp put_roles(params, roles) do
    # need to loop over the roles Enum
    Map.merge(params, %{"role_id" => roles.id})
  end
end
