defmodule ContentoWeb.WebsiteView do
  use ContentoWeb, :theme_view

  def page_title(:index, %{settings: %{website_description: desc}}), do: desc
  def page_title(:show_page, %{page: page}), do: page.title
  def page_title(:show_post, %{post: post}), do: post.title
end
