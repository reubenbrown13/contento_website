use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :contento, ContentoWeb.Endpoint,
  http: [port: 4002],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

# Configure your database
config :contento, Contento.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "postgres",
  password: "po5t6r35",
  database: "contento_dev",
  hostname: "localhost",
  port: 5434,
  pool: Ecto.Adapters.SQL.Sandbox
