defmodule Contento.Mixfile do
  use Mix.Project

  def project do
    [
      app: :contento,
      version: "0.0.1",
      elixir: "~> 1.5",
      elixirc_paths: elixirc_paths(Mix.env),
      compilers: [:phoenix, :gettext] ++ Mix.compilers,
      start_permanent: Mix.env == :prod,
      aliases: aliases(),
      deps: deps()
    ]
  end

  def application do
    [
      mod: {Contento.Application, []},
      extra_applications: [:logger, :runtime_tools]
    ]
  end

  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_),     do: ["lib"]

  defp deps do
    [
      {:credo, "~> 0.8", only: [:dev, :test], runtime: false},
      {:phoenix, "~> 1.5.0"},
      {:phoenix_pubsub, "~> 2.0"},
      {:phoenix_html, "~> 2.10"},
      {:phoenix_live_reload, "~> 1.0", only: :dev},
      {:ecto, "~> 3.5"},
      {:phoenix_ecto, "~> 4.0"},
      {:ecto_sql, "~> 3.5"},
      {:db_connection, "~> 2.2"},
      {:telemetry, "~> 0.4.0"},
      {:postgrex, "~> 0.15.0"},
      {:gettext, "~> 0.11"},
      {:plug, "~> 1.0"},
      {:cowboy, "~> 2.0"},
      {:plug_cowboy, "~> 2.1"},
      {:guardian, "~> 1.0-beta"},
      {:comeonin, "~> 4.0"},
      {:argon2_elixir, "~> 1.2"},
      {:proper_case, "~> 1.0.2"},
      {:timex, "~> 3.1"},
      {:earmark, "~> 1.2"},
      {:bamboo, "~> 0.8"},
      {:bamboo_smtp, "~> 1.4.0"},
      {:scrivener_ecto, "~> 2.0"},
      {:jason, "~> 1.1"},
      {:ex_doc, ">= 0.0.0", only: :docs}
    ]
  end

  defp aliases do
    [
      "ecto.setup": ["ecto.create", "ecto.migrate", "run priv/repo/seeds.exs"],
      "ecto.reset": ["ecto.drop", "ecto.setup"],
      "test": ["ecto.create --quiet", "ecto.migrate", "test"]
    ]
  end
end
